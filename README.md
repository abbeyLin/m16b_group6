# CP57 - A Multiplayer Exploration of the Skull in VR

We plan to build a VR Skull, that is interactive, and connected, for education. [More Information](https://news.microsoft.com/en-au/features/medical-imaging-paramedic-training-immersive-learning-era-mixed-reality/)

## Getting Started

Follow this instruction to get you a copy og the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Install Unity first, if not sure, can follow this [guide](https://docs.unity3d.com/Manual//GettingStartedInstallingHub.html).  

Open Unity Hub, in **Projects** add this repo, then Untiy should automatically generates Library based on the Packages/manifest.json. 

### Import Oculus Plugin

1. In the Unity menu select Window -> Asset Store.
2. In the Asset Store window that will pop up, type “Oculus Integration” in the search box that is in the upper part of the layout.
3. Download and Install the first result (a plugin made by Oculus, that is free).

### Unity Setting 
1. Open the **Edit** menu and pick **Project Settings → Editor**:
	2. Switch *Version Control Mode* to **Visible Meta** Files.
	3. Switch *Asset Serialization Mode* to **Force Text**.